# VPC

resource "aws_vpc" "anghami-vpc" {
    cidr_block = "10.0.0.0/16"
    instance_tenancy = "default"

    tags = {
        Name = "anghami-vpc"
    }
}

# SUBNET

resource "aws_subnet" "anghami-subnet-1" {
    vpc_id     = aws_vpc.anghami-vpc.id
    cidr_block = "10.0.1.0/24"
    map_public_ip_on_launch = "false"
    availability_zone = "us-east-1a"

    tags = {
        Name = "anghami-subnet-1"
    }
}

resource "aws_subnet" "anghami-subnet-2" {
    vpc_id     = aws_vpc.anghami-vpc.id
    cidr_block = "10.0.2.0/24"
    map_public_ip_on_launch = "false"
    availability_zone = "us-east-1b"

    tags = {
        Name = "anghami-subnet-2"
    }
}

# INTERNET GATEWAY

resource "aws_internet_gateway" "anghami-internet-gateway" {
    vpc_id = aws_vpc.anghami-vpc.id

    tags = {
        Name = "anghami-internet-gateway"
    }
}